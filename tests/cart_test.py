# mypy: no-disallow-untyped-decorators
# pylint: disable=E0611,E0401
import asyncio
from typing import Generator
from uuid import uuid4

import pytest
from fastapi.testclient import TestClient
from tortoise.contrib.test import finalizer, initializer

from settings.main import app
from src.models import Product

user_uuid = str(uuid4())


@pytest.fixture(scope='module')
def client() -> Generator:
    initializer(['src.models'])
    with TestClient(app) as c:
        yield c
    finalizer()


@pytest.fixture(scope='module')
def event_loop():
    yield asyncio.get_event_loop()


def test_get_cart(client: TestClient):
    """Should be return a full cart data."""
    response = client.get(f'/cart/{user_uuid}')
    assert response.status_code == 200
    data = response.json()
    assert data['user_uuid'] == user_uuid
    assert data['coupon_code'] == ''
    assert data['total'] == 0.0


@pytest.mark.anyio
async def test_add_item_to_cart(client: TestClient):
    """Should be add product in cart and return a full cart data"""
    product: Product = await Product.get(id=1)
    response = client.post(f'/cart/{user_uuid}', json={'id': product.id, 'quantity': 1})
    assert response.status_code == 200
    data = response.json()
    assert data['user_uuid'] == user_uuid
    assert len(data['products']) == 1
    assert data['coupon_code'] == ''
    assert data['total'] == product.price


@pytest.mark.anyio
async def test_update_item_to_cart(client: TestClient):
    """Should be update quantity product in cart and return a full cart data"""
    product: Product = await Product.get(id=1)
    quantity = 3
    response = client.put(
        f'/cart/{user_uuid}', json={'id': product.id, 'quantity': quantity}
    )
    assert response.status_code == 200
    data = response.json()
    assert data['user_uuid'] == user_uuid
    assert len(data['products']) == 1
    assert data['coupon_code'] == ''
    assert data['total'] == round(quantity * product.price, 2)


@pytest.mark.anyio
async def test_remove_item_to_cart(client: TestClient):
    """Should be remove product from cart and return a full cart data"""
    product: Product = await Product.get(id=1)
    response = client.delete(f'/cart/{user_uuid}/{product.id}')
    assert response.status_code == 200
    data = response.json()
    assert data['user_uuid'] == user_uuid
    assert len(data['products']) == 0
    assert data['coupon_code'] == ''


@pytest.mark.anyio
async def test_clear_to_cart(client: TestClient):
    """Should be remove all products from cart"""
    response = client.delete(f'/cart/{user_uuid}')
    assert response.status_code == 204


@pytest.mark.anyio
async def test_add_coupon_code_to_cart(client: TestClient):
    """Should be add coupon to cart and return a full cart data"""
    code = 'LOJA10'
    response = client.post(f'/cart/{user_uuid}/coupon', json={'code': code})
    assert response.status_code == 200
    assert response.json().get('coupon_code') == code
