from tortoise import fields, models


class Cart(models.Model):
    user_uuid = fields.UUIDField(pk=True, index=True)
    products = fields.JSONField(default={})
    coupon_code = fields.CharField(max_length=10, default='')

    async def add_product(self, product_id: str, quantity: int) -> None:
        self.products[product_id] = quantity
        await self.save()

    async def update_product(self, product_id: str, quantity: int) -> None:
        if product_id not in self.products.keys():
            return
        self.products[product_id] = quantity
        await self.save()

    async def remove_product(self, product_id: str) -> None:
        del self.products[product_id]
        await self.save()

    async def add_coupon_code(self, code: str) -> None:
        self.coupon_code = code
        await self.save()

    # class PydanticMeta:
    #     computed = ["name_length", "team_size", "not_annotated"]
    #     exclude = ["manager", "gets_talked_to"]
    #     allow_cycles = True
    #     max_recursion = 4
