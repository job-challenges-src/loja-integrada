from tortoise import fields, models


class Product(models.Model):
    id = fields.IntField(pk=True)
    title = fields.CharField(max_length=150)
    description = fields.TextField()
    price = fields.DecimalField(decimal_places=2, max_digits=8)
    stock = fields.IntField(default=0)
