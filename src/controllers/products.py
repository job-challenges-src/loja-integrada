from fastapi import HTTPException, status

from src.models import Product
from src.schemas import ProductListOut, ProductOut


async def list_products():
    return await ProductListOut.from_queryset(Product.all())


async def get_product(id: int):
    try:
        product = await Product.get(id=id)
        return await ProductOut.from_tortoise_orm(product)
    except Exception:
        raise HTTPException(
            detail='Product does not exists', status_code=status.HTTP_404_NOT_FOUND
        )
