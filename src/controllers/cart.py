from src.schemas import CartcouponCodeIn, ProductIn
from src.usercases.add_coupom_code_to_cart import add_coupon_to_cart_usercase
from src.usercases.add_item_to_cart import add_item_to_cart_usercase
from src.usercases.clear_cart import clear_cart_usercase
from src.usercases.get_full_cart import get_full_cart
from src.usercases.remove_item_from_cart import remove_item_to_cart_usercase
from src.usercases.update_items_from_cart import update_item_to_cart_usercase


async def get_cart_from_user_controller(user_uuid: str):
    return await get_full_cart(user_uuid)


async def add_item_to_cart_controller(user_uuid: str, product_in: ProductIn):
    return await add_item_to_cart_usercase(
        user_uuid, product_id=product_in.id, quantity=product_in.quantity
    )


async def update_item_to_cart_controller(user_uuid: str, product_in: ProductIn):
    return await update_item_to_cart_usercase(
        user_uuid, product_id=product_in.id, quantity=product_in.quantity
    )


async def remove_item_to_cart_controller(user_uuid: str, product_id: str):
    return await remove_item_to_cart_usercase(
        user_uuid,
        product_id=product_id,
    )


async def clear_cart_controller(user_uuid: str):
    return await clear_cart_usercase(user_uuid)


async def add_coupon_to_cart_controller(user_uuid: str, coupon: CartcouponCodeIn):
    return await add_coupon_to_cart_usercase(user_uuid, code=coupon.code)
