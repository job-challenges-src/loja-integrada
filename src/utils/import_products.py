import json

from src.models import Product


class ImportProducts:
    async def exec(self):
        with open('products.json', 'r') as file:
            products = json.loads(file.read())
            print('Criando produtos')
            for product_data in products:
                if not await Product.filter(title=product_data.get('title')).exists():
                    product = await Product.create(**product_data)
                    print('\t', product.title)
