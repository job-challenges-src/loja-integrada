from decimal import Decimal
from typing import List, Optional
from uuid import UUID

from pydantic import BaseModel

from src.models import Cart, Product

from . import CartOut, ProductOut


class _ProductOut(BaseModel):
    id: int
    title: str
    description: str
    price: Decimal
    stock: int


class _ProductDetailOut(BaseModel):
    detail: _ProductOut
    quantity: int
    total: Decimal
    available: bool


class CartFullOut(BaseModel):
    user_uuid: UUID
    products: List[_ProductDetailOut]
    coupon_code: Optional[str]
    total: Decimal

    @classmethod
    async def from_orm(cls, cart: Cart):
        products_id = list(map(int, cart.products.keys()))
        products = {
            str(product.id): {'detail': ProductOut.from_orm(product).dict()}
            for product in await Product.filter(id__in=products_id).order_by('id')
        }
        for pk, qtd in cart.products.items():
            price = products[pk]['detail']['price']
            products[pk]['quantity'] = qtd
            products[pk]['available'] = products[pk]['detail']['stock'] >= qtd
            products[pk]['total'] = Decimal(f'{qtd * price:.2f}')

        serialized_cart = CartOut.from_orm(cart).dict()
        serialized_cart['products'] = list(products.values())
        serialized_cart[
            'total'
        ] = Decimal(f'{sum(map(lambda p: float(p["total"]), products.values())):.2f}')
        return serialized_cart


class CartcouponCodeIn(BaseModel):
    code: str
