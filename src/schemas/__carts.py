from tortoise.contrib.pydantic import pydantic_model_creator, pydantic_queryset_creator

from src.models import Cart

CartIn = pydantic_model_creator(Cart, name='CartIn', exclude_readonly=True)
CartOut = pydantic_model_creator(Cart, name='CartOut')
CartListOut = pydantic_queryset_creator(Cart)
