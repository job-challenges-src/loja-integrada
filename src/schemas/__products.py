from pydantic import BaseModel
from tortoise.contrib.pydantic import pydantic_model_creator, pydantic_queryset_creator

from src.models import Product

ProductOut = pydantic_model_creator(Product, name='ProductOut')
ProductListOut = pydantic_queryset_creator(Product)


class ProductIn(BaseModel):
    id: str
    quantity: int
