from fastapi import APIRouter

from src.controllers.cart import (
    add_coupon_to_cart_controller,
    add_item_to_cart_controller,
    clear_cart_controller,
    get_cart_from_user_controller,
    remove_item_to_cart_controller,
    update_item_to_cart_controller,
)
from src.schemas import CartFullOut

_router = APIRouter()

_router.add_api_route(
    path='/cart/{user_uuid}',
    methods=['GET'],
    endpoint=get_cart_from_user_controller,
    response_model=CartFullOut,
    name='Get Cart',
    description='Returns full cart',
)

_router.add_api_route(
    path='/cart/{user_uuid}',
    methods=['POST'],
    endpoint=add_item_to_cart_controller,
    response_model=CartFullOut,
    name='Add item',
    description='Add an new item in cart',
)

_router.add_api_route(
    path='/cart/{user_uuid}',
    methods=['PUT'],
    endpoint=update_item_to_cart_controller,
    response_model=CartFullOut,
    name='Update item',
    description='Update an existing item in cart',
)

_router.add_api_route(
    path='/cart/{user_uuid}/{product_id}',
    methods=['DELETE'],
    endpoint=remove_item_to_cart_controller,
    name='Remove item',
    description='Remove an item in cart',
)

_router.add_api_route(
    path='/cart/{user_uuid}',
    methods=['DELETE'],
    endpoint=clear_cart_controller,
    name='Clear cart',
    description='Remove all items of card',
)

_router.add_api_route(
    path='/cart/{user_uuid}/coupon',
    methods=['POST'],
    endpoint=add_coupon_to_cart_controller,
    response_model=CartFullOut,
    name='Add coupon',
    description='Add or update an coupon code in cart',
)
