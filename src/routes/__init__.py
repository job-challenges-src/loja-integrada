from fastapi import APIRouter

from .__cart import _router as __cart_router
from .__products import _router as __product_router

route = APIRouter()

route.include_router(__product_router, tags=['Products'])
route.include_router(__cart_router, tags=['Cart'])
