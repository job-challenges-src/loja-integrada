from fastapi import APIRouter

from src.controllers.products import get_product, list_products

_router = APIRouter()

_router.add_api_route(path='/products', methods=['GET'], endpoint=list_products)

_router.add_api_route(path='/products/{id}', methods=['GET'], endpoint=get_product)
