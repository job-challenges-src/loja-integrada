from fastapi import HTTPException, status

from src.models import Cart, Product
from src.schemas import CartFullOut


async def add_item_to_cart_usercase(
    user_uuid: str, product_id: str, quantity: int
) -> CartFullOut:
    product = await Product.get_or_none(id=product_id)
    if not product:
        raise HTTPException(
            detail='Product not found',
            status_code=status.HTTP_404_NOT_FOUND,
        )

    if product.stock < quantity:
        raise HTTPException(
            detail='Product dont have stock',
            status_code=status.HTTP_422_UNPROCESSABLE_ENTITY,
        )

    cart, _ = await Cart.get_or_create(user_uuid=user_uuid)
    await cart.add_product(product_id, quantity)
    return await CartFullOut.from_orm(cart)
