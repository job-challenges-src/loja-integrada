from fastapi import HTTPException, status

from src.models import Cart, Product
from src.schemas import CartFullOut


async def update_item_to_cart_usercase(
    user_uuid: str, product_id: str, quantity: int
) -> CartFullOut:
    product = await Product.get_or_none(id=product_id)
    if not product:
        raise HTTPException(
            detail='Product not found',
            status_code=status.HTTP_404_NOT_FOUND,
        )

    if product.stock < quantity:
        raise HTTPException(
            detail='Product dont have stock',
            status_code=status.HTTP_422_UNPROCESSABLE_ENTITY,
        )
    cart: Cart = await Cart.get_or_none(user_uuid=user_uuid)
    if not cart:
        raise HTTPException(
            detail='Invalid cart!', status_code=status.HTTP_400_BAD_REQUEST
        )
    await cart.update_product(product_id, quantity)
    return await CartFullOut.from_orm(cart)
