from fastapi import status
from fastapi.responses import JSONResponse

from src.models import Cart


async def clear_cart_usercase(user_uuid: str):
    await Cart.filter(user_uuid=user_uuid).delete()
    return JSONResponse(content=None, status_code=status.HTTP_204_NO_CONTENT)
