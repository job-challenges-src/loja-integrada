from fastapi import HTTPException, status

from src.models import Cart
from src.schemas import CartFullOut


async def remove_item_to_cart_usercase(user_uuid: str, product_id: str) -> CartFullOut:
    cart: Cart = await Cart.get_or_none(user_uuid=user_uuid)
    if not cart:
        raise HTTPException(
            detail='Invalid cart!', status_code=status.HTTP_400_BAD_REQUEST
        )
    try:
        await cart.remove_product(product_id)
    except KeyError:
        raise HTTPException(
            detail='Product not in cart', status_code=status.HTTP_400_BAD_REQUEST
        )
    return await CartFullOut.from_orm(cart)
