from src.models import Cart
from src.schemas import CartFullOut


async def get_full_cart(user_uuid: str) -> CartFullOut:
    cart, _ = await Cart.get_or_create(user_uuid=user_uuid)
    return await CartFullOut.from_orm(cart)
