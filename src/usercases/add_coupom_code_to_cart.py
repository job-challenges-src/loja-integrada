from src.models import Cart
from src.schemas import CartFullOut


async def add_coupon_to_cart_usercase(user_uuid: str, code: str) -> CartFullOut:
    cart, _ = await Cart.get_or_create(user_uuid=user_uuid)
    await cart.add_coupon_code(code)
    return await CartFullOut.from_orm(cart)
