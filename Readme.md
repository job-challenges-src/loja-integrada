# Desafio carrinho de compras loja integrada

## Getting started

Para rodar o projeto você precisa ter instalado o `docker` e `docker-compose`.

Para iniciar o projeto você precisa executar somente um simples comando.

```bash
sudo docker-compose up
```
ou caso queira utilizar o alias do projeto.

```bash
make up
```

## Documentação
A documentação da api está disponível em

[Swagger](http://localhost:8000/docs) <br>
[Redoc](http://localhost:8000/redoc)