FROM python:3.10-alpine

ENV PYTHONUNBUFFERED=1

RUN apk update
RUN apk add bash
RUN apk add git

RUN mkdir app

WORKDIR /app
COPY .env.dev /app/.env
COPY . /app
ADD . /app/

RUN pip install -r dev-requirements.txt
RUN git init && pre-commit install

EXPOSE 8000
