from fastapi import FastAPI
from tortoise.contrib.fastapi import register_tortoise

from src.routes import route
from .config import settings

app = FastAPI()
app.include_router(route)

register_tortoise(
    app,
    db_url=settings.DATABASE_URL,
    modules={'models': ['src.models']},
    generate_schemas=True,
    add_exception_handlers=True,
)


@app.on_event('startup')
async def dump():
    from src.utils import ImportProducts
    await ImportProducts().exec()
